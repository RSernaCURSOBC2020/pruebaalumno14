page 50102 "TCN_ListaAves"
{
    PageType = List;
    SourceTable = TCN_Ave;
    Caption = 'Lista Aves';
    ApplicationArea = All;
    UsageCategory = Tasks;
    CardPageId = TCN_FichaAves;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codigo; Codigo)
                {
                    ApplicationArea = All;
                }
                field(Nombre; Nombre)
                {
                    ApplicationArea = All;
                }
                field(Preguntar; Preguntar)
                {
                    ApplicationArea = All;
                }
            }
        }
    }

}
