page 50110 "TCN_ListaLineas"
{
    Caption = 'Lista lineas';
    PageType = ListPart;
    SourceTable = TCN_Lineas;
    AutoSplitKey = true;
    PopulateAllFields = true;

    layout
    {
        area(Content)
        {
            repeater(Group)
            {
                field(Codigo; Codigo)
                {
                    ApplicationArea = All;
                }
                field(DesVenta; DesVenta)
                {
                    ApplicationArea = All;
                }
                field(FechaVenta; FechaVenta)
                {
                    ApplicationArea = All;
                }
                field(NumLinea; NumLinea)
                {
                    ApplicationArea = All;
                }
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(ActionName)
            {
                ApplicationArea = All;

                trigger OnAction()
                begin

                end;
            }
        }
    }
}