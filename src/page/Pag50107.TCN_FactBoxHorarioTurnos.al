page 50107 "TCN_FactBoxHorarioTurnos"
{
    Caption = 'Horario Turnos';
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = TCN_Turno;
    CardPageId = TCN_FichaTurno;
    layout
    {
        area(Content)
        {

        }
        area(FactBoxes)
        {
            part(InfTurno; TCN_FactBoxTurnos)
            {
                SubPageLink = Codigo = field (Codigo);
            }

            part(Horarios; TCN_FactBoxTurnos)
            {
                SubPageLink = CodTurno = field (Codigo);
            }
        }
    }

}