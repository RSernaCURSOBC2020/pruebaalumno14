page 50104 "TCN_PedirTexto"
{
    Caption = 'Pedir texto';
    PageType = Card;
    SourceTable = Integer;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTableView = where (Number = const (0));

    layout
    {

        area(Content)
        {
            group(GroupName)
            {
                field(TextoAIntroducir; xTexto)
                {
                    Caption = 'Texto a introducir';
                    ApplicationArea = All;
                }
            }
        }
    }

    var
        xTexto: Text;

}