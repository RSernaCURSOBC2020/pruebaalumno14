page 50111 "TCN_Cabecera"
{
    Caption = 'Cabecera';
    PageType = Card;
    ApplicationArea = All;
    UsageCategory = Documents;
    SourceTable = TCN_Cabecera;

    layout
    {
        area(Content)
        {
            group(General)
            {
                field(Codigo; Codigo)
                {
                    ApplicationArea = All;

                }
                field(Descripcion; Descripcion)
                {
                    ApplicationArea = All;
                }
            }
            group(Informacion)
            {
                field(DescripcionInfo; Descripcion)
                {
                    ApplicationArea = All;
                }
            }

            part(Lineas; TCN_ListaLineas)
            {
                SubPageLink = Codigo = field (Codigo);
            }
        }
    }

    actions
    {
        area(Processing)
        {
            action(Mensaje)
            {
                ApplicationArea = All;

                trigger OnAction()
                begin
                    Message('Hola clase');
                end;
            }
        }

        area(Navigation)
        {
            action(ExtractoClie)
            {
                Caption = 'Extracto cliente';
                Image = AddContacts;
                RunObject = page "Company Information";
            }

            action(ClientesVarios)
            {
                Caption = 'Clientes varios';
                Image = Customer;
                RunObject = page "Customer List";
                RunPageLink = "Country/Region Code" = filter ('*ES*');

                /*trigger OnAction()
                begin
                    Message('pagina modificada');
                end;*/
            }

            action(PedirDatos)
            {
                Caption = 'Pedir datos';
                Image = Absence;
                RunObject = page TCN_PedirTexto;
            }

            action(CreditoGrupoCliente)
            {
                Caption = 'Credito por grupo cliente';
                Image = Action;

                trigger OnAction()

                var
                    rlCustomer: Record Customer;
                    rlCustomerTemp: Record Customer temporary;

                begin
                    rlCustomer.FindSet(false);

                    repeat
                        rlCustomerTemp.SetRange("Customer Posting Group", rlCustomer."Customer Posting Group");

                        if not rlCustomerTemp.FindFirst() then begin
                            rlCustomerTemp.Init();
                            rlCustomerTemp."No." := rlCustomer."Customer Posting Group";
                            rlCustomerTemp."Customer Posting Group" := rlCustomer."Customer Posting Group";
                            rlCustomerTemp.Insert(false);
                        end;
                        rlCustomerTemp."Credit Limit (LCY)" := rlCustomer."Credit Limit (LCY)";
                        rlCustomerTemp.Modify(false);
                    until rlCustomer.Next() = '0';

                    page.Run(page::TCN_CustomerTMP, rlCustomerTemp);
                end;
            }
        }
    }


}