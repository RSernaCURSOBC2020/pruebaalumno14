page 50109 "TCN_FactBoxTurnos"
{
    Caption = 'FactBoxTurnos';
    PageType = CardPart;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = TCN_Turno;

    layout
    {
        area(Content)
        {
            group(Grupo)
            {
                field(InfTurnos; InfTurnos)
                {
                    ApplicationArea = All;
                }

            }
        }
    }

    actions
    {
        area(Processing)
        {

        }
    }
}