page 50100 "TCN_ListaA"
{

    PageType = List;
    SourceTable = TCN_a;
    Caption = 'TCN_ListaA';
    ApplicationArea = All;
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Color; Color)
                {
                    ApplicationArea = All;
                }
                field(DiaSemana; DiaSemana)
                {
                    ApplicationArea = All;
                }
                field(Talla; Talla)
                {
                    ApplicationArea = All;
                }
                field(Descripcion; Descripcion)
                {
                    ApplicationArea = All;
                }
                field(NumCliente; NumCliente)
                {
                    ApplicationArea = All;
                }
                field(NombreCliente; NombreCliente)
                {
                    ApplicationArea = All;
                }
                field(NumPersona; NumPersona)
                {
                    ApplicationArea = All;
                }
                field(TipoPersona; TipoPersona)
                {
                    ApplicationArea = All;
                }
                field(SumaImporte; SumaImporte)
                {
                    ApplicationArea = All;
                }
                field(CodAve; CodAve)
                {
                    ApplicationArea = All;
                }

            }
        }
    }

    actions
    {
        area(Creation)
        {
            action(SetValor)
            {
                trigger OnAction()
                begin
                    cuRegistrosVarios.SetValorF('Hola curso');
                end;
            }

            action(GetValor)
            {
                trigger OnAction()
                begin
                    Message(cuRegistrosVarios.GetValorF());
                end;
            }
        }
    }

    var
        cuRegistrosVarios: Codeunit TCN_RegistrosVarios;
}
