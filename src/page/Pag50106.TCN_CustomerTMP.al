page 50106 "TCN_CustomerTMP"
{
    Caption = 'Temporal de clientes';
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = Customer;
    SourceTableTemporary = true;

    layout
    {
        area(Content)
        {
            repeater(GroupName)
            {

                field("Customer Posting Group"; "Customer Posting Group")
                {
                    ApplicationArea = All;
                }

                field("Credit Limit (LCY)"; "Credit Limit (LCY)")
                {
                    ApplicationArea = All;
                }
            }
        }
    }

}