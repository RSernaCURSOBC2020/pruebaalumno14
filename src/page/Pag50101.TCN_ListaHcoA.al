page 50101 "TCN_ListaHcoA"
{
    Caption = 'Lista historico de A';
    PageType = List;
    ApplicationArea = All;
    UsageCategory = Lists;
    SourceTable = TCN_HcoA;

    layout
    {
        area(Content)
        {
            group(GroupG)
            {
                field(Color; Color)
                {
                    ApplicationArea = All;
                }
                field(DiaSemana; DiaSemana)
                {
                    ApplicationArea = All;
                }
                field(FechaRegistro; FechaRegistro)
                {
                    ApplicationArea = All;
                }
                field(Talla; Talla)
                {
                    ApplicationArea = All;
                }
            }
        }
    }

    actions
    {
        area(Creation)
        {
            action(SetValor)
            {
                trigger OnAction()
                begin
                    cuRegistrosVarios.SetValorF('Hola curso');
                end;
            }

            action(GetValor)
            {
                trigger OnAction()
                begin
                    Message(cuRegistrosVarios.GetValorF());
                end;
            }
        }
    }

    var
        cuRegistrosVarios: Codeunit TCN_RegistrosVarios;
}