codeunit 50100 "TCN_RegistrosVarios"
{
    Permissions = tabledata "Sales Invoice Header" = mid,
                  tabledata "Sales Invoice Line" = mid;

    SingleInstance = true;

    var
        xValor: Text;

    procedure SetValorF(pValor: Text)
    begin
        xValor := pValor;
        Message('Valor %1', xValor);
    end;

    procedure GetValorF(): Text
    begin
        exit(xValor);
    end;

    local procedure RegistrarEntrada() xSalida: Integer
    begin

    end;

    [TryFunction]
    local procedure EjemploTry()
    begin
    end;
}