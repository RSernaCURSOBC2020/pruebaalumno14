codeunit 50101 "TCN_SuscripcionesVar"

{
    SingleInstance = true;
    EventSubscriberInstance = Manual;

    [EventSubscriber(ObjectType::Table, Database::Customer, 'OnAfterInsertEvent', '', false, false)]
    local procedure MyProcedure(var Rec: Record Customer)
    begin
        if not Confirm(StrSubstNo('¿Desea insertar el cliente %1?', Rec."No.", true)) then begin
            Error('Proceso cancelado por el usuario');
        end;
    end;


    [EventSubscriber(ObjectType::Table, Database::TCN_Lineas, 'onAfterVerificarDatosLineasF',
         '', false, false)]
    local procedure onAfterVerificarDatosLineasFTableTCNLineasF(var RecLineas: Record TCN_Lineas; xRecLineas: Record TCN_Lineas)
    begin
        if RecLineas.DesVenta = ' ' then begin
            RecLineas.DesVenta := 'Sin descripcion';
        end;

        if RecLineas.DesVenta = xRecLineas.DesVenta then begin
            RecLineas.DesVenta := 'Misma descripcion';
        end;
    end;

    [EventSubscriber(ObjectType::Table, Database::Item, 'OnBeforeValidateEvent', 'Description', false, false)]
    local procedure OnBeforeValidateEventDescriptionTableItemF()
    begin

    end;
}