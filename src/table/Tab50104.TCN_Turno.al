table 50104 "TCN_Turno"
{
    DataClassification = ToBeClassified;

    fields
    {
        field(1; "InfTurnos"; Text[2048])
        {
            Caption = 'InfTurnos';
            DataClassification = ToBeClassified;
        }
        field(2; "Codigo"; Code[50])
        {
            Caption = 'Código';
        }
        field(3; "CodTurno"; Code[50])
        {
            Caption = 'Código Turno';
        }

    }

    keys
    {
        key(PK; "InfTurnos")
        {
            Clustered = true;
        }
    }

}