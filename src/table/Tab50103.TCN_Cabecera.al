table 50103 "TCN_Cabecera"
{
    DataClassification = ToBeClassified;

    fields
    {
        field(1; "Codigo"; Code[10])
        {
            Caption = 'Codigo';
            DataClassification = ToBeClassified;
        }
        field(2; "Descripcion"; Text[50])
        {
            Caption = 'Descripcion';
            DataClassification = ToBeClassified;
        }

    }

    keys
    {
        key(PK; "Codigo")
        {
            Clustered = true;
        }
    }

}