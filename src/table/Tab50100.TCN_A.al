table 50100 "TCN_A"
{
    DataClassification = ToBeClassified;

    Caption = 'Tabla A';
    LookupPageId = TCN_ListaA;
    DrillDownPageId = TCN_ListaA;

    fields
    {
        field(1; "Color"; Enum TCN_Colores)
        {

            DataClassification = ToBeClassified;
        }

        field(2; "Talla"; Enum TCN_Tallas)
        {
            DataClassification = ToBeClassified;
        }


        field(3; "DiaSemana"; Option)
        {
            OptionMembers = Lunes,Martes,Miercoles,Jueves;
        }

        field(4; "NumCliente"; Code[20])
        {
            Caption = 'Numero cliente';
            TableRelation = Customer."No."; //where ("Phone No." = filter (<> ' '));
            DataClassification = ToBeClassified;

            trigger OnValidate()
            begin
                Message('Has elegido el cliente %1', NumCliente);
            end;
        }
        field(5; "Descripcion"; Text[250])
        {
            Caption = 'Descripcion';
            DataClassification = ToBeClassified;

            trigger OnValidate()
            begin
                Descripcion := UpperCase(Descripcion);
            end;
        }

        field(6; "TipoPersona"; Enum TCN_TipoPersona)
        {
            Caption = 'Cliente o proveedor';
            InitValue = 'Cliente';
            DataClassification = ToBeClassified;
        }

        field(7; "NumPersona"; Code[20])
        {
            Caption = 'Nº cte/prov';
            InitValue = ' ';
            TableRelation = if (TipoPersona = const (Cliente)) Customer."No."
            else
            Vendor."No.";
        }

        field(8; "SumaImporte"; Decimal)
        {
            Caption = 'Suma Importe';
            FieldClass = FlowField;
            CalcFormula = sum ("Sales Line".Amount where ("Sell-to Customer No." = field (NumCliente),
            "Shipment Date" = field (FiltroFecha), "No." = field (FiltroProducto)));
        }

        field(9; "FiltroFecha"; Date)
        {
            Caption = 'Filtro fecha de envio';
            FieldClass = FlowFilter;
        }

        field(10; FiltroProducto; Code[20])
        {
            Caption = 'Filtro de producto';
            FieldClass = FlowFilter;
            TableRelation = "Item"."No.";
        }

        field(11; "NombreCliente"; Text[2048])
        {
            Caption = 'Nombre cliente';
            FieldClass = FlowField;
            CalcFormula = lookup (Customer.Name where ("No." = field (NumCliente)));
        }

        field(12; "CodAve"; Code[10])
        {
            Caption = 'Codigo Ave';
            TableRelation = TCN_Ave.Codigo;
            DataClassification = ToBeClassified;
        }
    }

    keys
    {
        key(PK; Color)
        {
            Clustered = true;
        }
        key(Secundaria; NumCliente, Color, DiaSemana)
        {

        }
    }

    trigger OnInsert()
    begin
        Message('Registro creado');
    end;

    trigger OnRename()
    begin
        Error('No se permite cambio de clave');
    end;
}