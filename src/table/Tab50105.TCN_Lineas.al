table 50105 "TCN_Lineas"
{
    DataClassification = ToBeClassified;

    fields
    {
        field(1; "Codigo"; Code[10])
        {
            Caption = 'Codigo';
            DataClassification = ToBeClassified;
            TableRelation = TCN_Cabecera.Codigo;
        }

        field(2; "NumLinea"; Integer)
        {
            Caption = 'Numero de Linea';
            DataClassification = ToBeClassified;
        }

        field(3; "FechaVenta"; Date)
        {
            Caption = 'Fecha de Venta';
            DataClassification = ToBeClassified;
        }

        field(4; "DesVenta"; Text[250])
        {
            Caption = 'Descripcion Venta';
            DataClassification = ToBeClassified;
        }

    }

    keys
    {
        key(PK; "Codigo", NumLinea)
        {
            Clustered = true;
        }
    }

    local procedure VerificarDatosF()
    var
        xlTexto: text;
    begin
        onBeforeVerificarDatosLineasF();



        onAfterVerificarDatosLineasF(Rec, xRec);

    end;

    [IntegrationEvent(false, true)]
    local procedure onBeforeVerificarDatosLineasF()
    begin

    end;

    [IntegrationEvent(false, true)]
    local procedure onAfterVerificarDatosLineasF(var RecLineas: Record TCN_Lineas; xRecLineas: Record TCN_Lineas)
    begin

    end;

    trigger OnInsert()
    begin
        VerificarDatosF();
    end;

    var
        xTextoGlobal: Text;

        CmsgError: Label 'Proceso cancelado';

        xVariable: array[10, 10] of Text;

}