tableextension 50101 "TCN_ItemExtPru" extends Item //MyTargetTableId
{
    fields
    {
        field(50100; CodAlmacen2; code[10])
        {
            Caption = 'Cód. almacén';
            TableRelation = Location.Code;
        }
    }

}